---
title: Retriggering an MR pipeline
linkTitle: Retrigger MR pipeline
description: How to get the bot to trigger a pipeline for a revision from an MR
---

## Problem

You want to trigger a pipeline via the bot for a specific revision from an MR
pipeline. As an example, [kernel-tests!802] needs to be tested against the
builds from [cki-internal-contributors pipeline 309536375].

## Steps

1. Determine the branch in the pipeline repository that was used for the
   pipeline from the pipeline overview page. For the pipeline above, that is
   `rhel8`.

1. From the [external-triggers pipelines], filter for the same branch name and
   select a suitable baseline pipeline. For the pipeline above,
   [external-triggers pipeline 300641680] seems like a good match.

1. Open the trigger variables of the pipeline. For that, select any job of the
   original pipeline, and click on the "Variables API URL" at the top of the
   console output. Copy the `git_url` and `commit_hash` variable values.

1. Ask the bot to trigger a pipeline with those values via

   ```plain
   @cki-ci-bot, please test
   [cki/300641680]
   [git_url=https://gitlab.com/redhat/rhel/src/kernel/rhel-8]
   [commit_hash=14e4040a03f3dfc822c88372b185d900a1ae3b65]
   [tests_only=false]
   ```

   Add more variables as needed, e.g. `skip_beaker` or `tests_regex`.

   The `tests_only` variable needs to be set to `false` to rebuild the revision
   from the provided `git_url` and `commit_hash`. Otherwise, the bot would use
   the artifacts from the original pipeline.

## Additional steps to reuse the artifacts

1. From the original pipeline, select all the publish jobs one-by-one and copy
   the job API URLs from the top of the console output. For each, append
   `/artifacts` and add the following `skip_*` and `ARTIFACT_URL_*` variables
   to the bot:

   ```plain
   [skip_merge=true]
   [skip_build=true]
   [skip_publish=true]
   [ARTIFACT_URL_aarch64=https://gitlab.com/api/v4/projects/20615920/jobs/1293587158/artifacts]
   [ARTIFACT_URL_ppc64le=https://gitlab.com/api/v4/projects/20615920/jobs/1293587159/artifacts]
   [ARTIFACT_URL_s390x=https://gitlab.com/api/v4/projects/20615920/jobs/1293587160/artifacts]
   [ARTIFACT_URL_x86_64=https://gitlab.com/api/v4/projects/20615920/jobs/1293587157/artifacts]
   ```

[kernel-tests!802]: https://gitlab.com/cki-project/kernel-tests/-/merge_requests/802
[cki-internal-contributors pipeline 309536375]: https://gitlab.com/redhat/red-hat-ci-tools/kernel/cki-internal-pipelines/cki-internal-contributors/-/pipelines/309536375
[external-triggers pipelines]: https://gitlab.com/redhat/red-hat-ci-tools/kernel/cki-internal-pipelines/external-triggers/-/pipelines
[external-triggers pipeline 300641680]: https://gitlab.com/redhat/red-hat-ci-tools/kernel/cki-internal-pipelines/external-triggers/-/pipelines/300641680
